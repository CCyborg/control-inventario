const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const Usuarios = require("../modules/Usuarios");
<<<<<<< HEAD
const TipoUsarios = require('../modules/TipoUsuarios');
=======
const TipoUsuarios = require("../modules/TipoUsuarios");
const repuestaError = require("../utils/respuestaError");

>>>>>>> 0af66e0a25a09837651f8a7628aa288aad02df62

//@route    POST api/auth/
//@desc     Iniciar sesión
//@access   Public
exports.iniciarSesion = async (req, res) => {
  const { email, contrasena } = req.body;
  try {
    const usuario = await Usuarios.obtenerUsuarioPorEmail(email);
    if (!usuario)
<<<<<<< HEAD
      return res.status(400).json({ msg: "Credenciales no válidas" });
=======
      return res.status(400).json({
        mensaje: "Credenciales no válidas",
        errores: [
          {
            msg: "Email y/o contaseña incorrectos.",
          },
        ],
      });
>>>>>>> 0af66e0a25a09837651f8a7628aa288aad02df62
    const coincide = await bcrypt.compare(contrasena, usuario.contrasena);
    const tipo =await TipoUsarios.obtenerDescripcion(usuario.idTipoUsuario);
    if (!coincide)
<<<<<<< HEAD
      return res.status(400).json({ msg: "Credenciales no válidas" });
=======
      return res.status(400).json({
        mensaje: "Credenciales no válidas",
        errores: [
          {
            msg: "Email y/o contaseña incorrectos.",
          },
        ],
      });
>>>>>>> 0af66e0a25a09837651f8a7628aa288aad02df62
    jwt.sign(
      {
        id: usuario.id,
        tipo:tipo.descripcion
      },
      process.env.JWT_SECRET,
      {expiresIn:process.env.JWT_EXPIRES_IN},
      (error, token) => {
        if (error) throw error;
        return res.status(201).json({
          msg: "Login exitoso",
<<<<<<< HEAD
          token,
          id:usuario.id,
          nombre:usuario.nombre,
          tipo:tipo.descripcion
=======
          token
>>>>>>> 0af66e0a25a09837651f8a7628aa288aad02df62
        });
      }
    );
  } catch (e) {
    console.log(e);
    res.status(500).send("Server error");
  }
};
<<<<<<< HEAD
=======

exports.obtenerUsuarioPorToken = async (req, res) => {
  const id = req.usuario.id;
  try {
    const usuario = await Usuarios.obtenerUsuarioPorId(id);
    if (!usuario)
      return respuestaError(
        400,
        "id de usuario no válido",
        [{ mensaje: "El id especificado no es válido" }],
        res
      );
    return res.json(usuario[0]);
  } catch (e) {
    console.log(e);
    return repuestaError(
      500,
      "Error de servidor",
      [{ mensaje: "Error intentando obtener el usuario" }],
      res
    );
  }
};
>>>>>>> 0af66e0a25a09837651f8a7628aa288aad02df62
